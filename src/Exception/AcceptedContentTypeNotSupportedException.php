<?php

namespace Bleicker\Controller\Exception;

use Bleicker\Exception\ThrowableException as Exception;

/**
 * Class AcceptedContentTypeNotSupportedException
 *
 * @package Bleicker\Controller\Exception
 */
class AcceptedContentTypeNotSupportedException extends Exception {

}
